package budecan.catalin.vasilica.lab6.ex2;

import budecan.catalin.vasilica.lab6.ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String owner, int balance){
        BankAccount account = new BankAccount(owner, balance);
        accounts.add(account);
    }

    public void printAccounts(){

        //ascending sort with encapsulation
        Collections.sort(accounts, (a1, a2) -> Double.valueOf(a1.getBalance()).compareTo(a2.getBalance()));

        //print out
        for(int i = 0; i<accounts.size(); i++ ){
            System.out.println("Owner's name: "+accounts.get(i).getOwner()+" with a balance of: "+accounts.get(i).getBalance());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        //ascending sort with lambda, java >8
        Collections.sort(accounts, (a1, a2) -> Double.valueOf(a1.getBalance()).compareTo(a2.getBalance()));

        //print out
        for(int i = 0; i<accounts.size(); i++ ){
            if(accounts.get(i).getBalance()>=minBalance && accounts.get(i).getBalance()<=maxBalance)
                System.out.println("Owner's name:"+accounts.get(i).getOwner()+" with a balance of:"+accounts.get(i).getBalance());
        }
    }

    public ArrayList<BankAccount> getAllAccounts() {
        return accounts;
    }
}