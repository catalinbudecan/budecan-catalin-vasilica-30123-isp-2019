package budecan.catalin.vasilica.lab6.ex2;

import budecan.catalin.vasilica.lab6.ex1.BankAccount;

import java.util.Collections;
import java.util.Comparator;

public class TestBank {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Popescu",2000);
        bank.addAccount("Ionescu",1000);
        bank.addAccount("Petrescu",3000);
        bank.addAccount("Placintar",20);

        bank.printAccounts();
    }


    void printAllAccounts(Bank bank){

        Collections.sort(bank.getAllAccounts(), new Comparator<BankAccount>(){
            public int compare(BankAccount a1, BankAccount a2){
                return String.valueOf(a1.getOwner()).compareTo(a2.getOwner());
            }
        });

        for(int i = 0; i<bank.getAllAccounts().size(); i++ ){
            System.out.println("Owner's name: "+bank.getAllAccounts().get(i).getOwner()+" with a balance of: "+bank.getAllAccounts().get(i).getBalance());
        }
    }
}