package budecan.catalin.vasilica.lab6.ex3;

public class TestBank2 {
    public static void main(String[] args) {
        Bank2 bank2 = new Bank2();
        bank2.addAccount("test2", 20000);
        bank2.addAccount("test3", 2000);
        bank2.addAccount("test4", 200);
        bank2.addAccount("test5", 200);
        bank2.addAccount("test6", 20);

        bank2.printAccounts();
    }
}