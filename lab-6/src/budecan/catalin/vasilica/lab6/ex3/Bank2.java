package budecan.catalin.vasilica.lab6.ex3;

import java.util.Set;
import java.util.TreeSet;

public class Bank2 {

    Set<BankAccount2> accounts = new TreeSet<BankAccount2>().descendingSet();


    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount2(owner, balance));
    }

    public void printAccounts() {
        for (BankAccount2 acc : accounts) {
            System.out.println(acc.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount2 acc : accounts) {
            if (acc.getBalance() >= minBalance && acc.getBalance() <= maxBalance)
                System.out.println(acc.toString());
        }
    }

    public Set<BankAccount2> getAllAccounts() {
        return accounts;
    }




}

