package budecan.catalin.vasilica.lab6.ex4;

public class Word{
    private String name;

    public Word(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return this.name;
    }

    public static boolean equalsW(Word w1, Word w2){
        if(w1.getName().equals(w2.getName())) return true;
        else return false;
    }
}