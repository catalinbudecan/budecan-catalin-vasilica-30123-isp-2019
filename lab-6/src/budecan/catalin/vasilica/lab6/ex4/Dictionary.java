package budecan.catalin.vasilica.lab6.ex4;

import java.util.Iterator;
import java.util.Map;

public class Dictionary extends HashM {

    public void addWord(Word word,Definition definition){
        hashM.put(word,definition);
    }

    public Definition getDefinition(Word word){
        Map mp = hashM;
        Iterator iter = mp.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry pair = (Map.Entry) iter.next();
            if (Word.equalsW(word, (Word) pair.getKey()) == true) return (Definition) pair.getValue();
        }
        System.out.println("Nu exista cuvantul.");
        return null;
    }



    public void getAllWords(){
        Map mp = hashM;
        Iterator iter = mp.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry pair = (Map.Entry)iter.next();
            System.out.println(pair.getKey());
        }
    }

    public void getAllDefinitions(){
        Map mp = hashM;
        Iterator iter = mp.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry pair = (Map.Entry)iter.next();
            System.out.println(pair.getValue());
        }
    }


}