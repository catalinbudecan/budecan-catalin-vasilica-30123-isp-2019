package budecan.catalin.vasilica.lab6.ex4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        while (true) {
//            System.out.println("\n Dictionar v0.0.0.0.0.0.0.0.0.0.1:pre-alpha");
            System.out.println("\n Selectati o optiune:"
                    + "\n\n+addWord(1)"
                    + "\n+getDefinition(2)"
                    + "\n+getAllWords(3)"
                    + "\n+getAllDefinitions(4)"
                    + "\n+Iesire(Any other key)"
            );

            System.out.println("Optiune aleasa:");
            Scanner scan = new Scanner(System.in);
            int choice = scan.nextInt();


            switch (choice) {
                case 1:
                    System.out.println("\n\nIntrodu cuvantul:");
                    String inputW = scan.next();
                    System.out.println("\nIntrodu definitia:");
                    String inputD = scan.next();
                    d.addWord(new Word(inputW), new Definition(inputD));
                    break;
                case 2:
                    System.out.println("\n\nIntrodu cuvantul:");
                    System.out.println(d.getDefinition(new Word(scan.next())));
                    break;
                case 3:
                    d.getAllWords();
                    break;
                case 4:
                    d.getAllDefinitions();
                    break;
                default:
                    return;
            }
        }
    }
}