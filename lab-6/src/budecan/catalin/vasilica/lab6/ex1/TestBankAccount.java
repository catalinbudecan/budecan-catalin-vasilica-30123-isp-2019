package budecan.catalin.vasilica.lab6.ex1;

public class TestBankAccount {
    public static void main(String[] args){
        BankAccount b1 = new BankAccount("Popescu",2000);
        BankAccount b2 = new BankAccount("Enache",2000);

        BankAccount b4 = new BankAccount("Popescu",2000);
        BankAccount b5 = new BankAccount("Popescu",1000);

        if(b1==b2) System.out.println("b1 == b2");
        if(b1.equals(b2)) System.out.println("b1 equals b2");

        if(b1==b1) System.out.println("b1 == b1");
        if(b1.equals(b1)) System.out.println("b1 equals b1");

        if(b1==b4) System.out.println("b1 == b4");
        if(b1.equals(b4)) System.out.println("b1 equals b4");

        if(b1==b5) System.out.println("b1 == b5");
        if(b1.equals(b5)) System.out.println("b1 equals b5");
    }
}