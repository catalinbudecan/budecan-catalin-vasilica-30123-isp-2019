package budecan.catalin.vasilica.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args) {
        Author A = new Author("Catalin", "ctlbudecan@gmail.com", 'm');
        System.out.println("The email of the author is " + A.getEmail());
        A.setEmail("catalin.budecan@gmail.com");
        System.out.println("The name of the author is " + A.getName());
        System.out.println("The email of the author is " + A.getEmail());
        System.out.println("The gender of the author is " + A.getGender());
        System.out.println(A.toString());
    }
}
