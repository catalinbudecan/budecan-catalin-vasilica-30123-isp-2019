package budecan.catalin.vasilica.lab2.ex2;
import java.util.Scanner;

public class ConvertInLetter {
    public static void main(String[] args) {
        Scanner  num = new Scanner(System.in);
        System.out.println("Number? ");
        int number = num.nextInt();
        String letters;
        switch (number) {
            case 0:
                letters = "ZERO";
                break;
            case 1:
                letters = "ONE";
                break;
            case 2:
                letters = "TWO";
                break;
            case 3:
                letters = "THREE";
                break;
            case 4:
                letters = "FOUR";
                break;
            case 5:
                letters = "FIVE";
                break;
            case 6:
                letters = "SIX";
                break;
            case 7:
                letters = "SEVEN";
                break;
            case 8:
                letters = "EIGHT";
                break;
            case 9:
                letters = "NINE";
                break;
            default:
                letters = "OTHER";
                break;
        }
        System.out.println(letters);
    }
}

