package budecan.catalin.vasilica.lab2.ex6.a;
import java.util.Scanner;

public class NonRecursive {
        public static void main(String args[]){
            int i,fact=1;
            Scanner  num = new Scanner(System.in);
            System.out.println("Which is the number?");
            int N = num.nextInt();
            for(i=1;i<=N;i++){
                fact=fact*i;
            }
            System.out.println("Factorial of "+N+" is: "+fact);
        }
}
