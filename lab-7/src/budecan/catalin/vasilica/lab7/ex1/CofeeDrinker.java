package budecan.catalin.vasilica.lab7.ex1;

public class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, NumberException {
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        if(Cofee.numb > 1)
            throw new NumberException(Cofee.numb,Cofee.numb+" coffees created.");
        System.out.println("Drink cofee:"+c);
    }
}