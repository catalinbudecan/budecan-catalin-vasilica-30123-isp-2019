package budecan.catalin.vasilica.lab7.ex2;

import java.io.*;
import java.util.Scanner;

public class CountStringInFile {
    public static void main(String args[]) throws Exception {

        Scanner scan = new Scanner(System.in);
        System.out.println("Introdu ce vrei sa caut:");

        String inString = scan.nextLine();
        String filePath = ".\\src\\budecan\\catalin\\vasilica\\lab7\\ex2\\DataIn\\data.txt";
        String strLine;

        int count = 0;

        try {
            FileInputStream fstream = new FileInputStream(filePath);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            while ((strLine = br.readLine()) != null) {
                strLine = strLine + " ";
                String[] strArry = strLine.split(inString);

                if (strArry.length > 1) {
                    count = count + strArry.length - 1;
                } else {
                    if (strLine == inString) {
                        count++;
                    }
                }
            }

            in.close();

            System.out.println("Cuvantul:"+inString+" a fost gasit de:"+count+" ori.");

        } catch (Exception e) {
            System.out.println("Fisierul nu a putut fi deschis.");
        }
    }
}